package com.in28minutes.rest.webservices.restfulwebservices.helloworld;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldControllerD {
	@Autowired
	private MessageSource messageSource;
	
//	@GetMapping(path = "/hello-world-bean")
//	public HelloWorldBean helloWorldBean() {
//		return new HelloWorldBean("Hello World");
//	}
	
	//hello-world/path-variable/in28minutes
	//this is for lesson 31: internationalization.
	//this method is before internationalization.
//	@GetMapping(path = "/hello-world-internationalized")
//	public String helloWorldInternationalized() {
//		return "Good Morning";
//	}
	
	//previous version (lesson 32)
//	@GetMapping(path = "/hello-world-internationalized")
//	public String helloWorldInternationalized(@RequestHeader(name="Accept-Language", required=false) Locale locale) {
//		return messageSource.getMessage("good.morning.message", null, 
//									     locale);
//	}
	
	//improvement (lesson 33)
	@GetMapping(path = "/hello-world-internationalized")
	public String helloWorldInternationalized() {
		return messageSource.getMessage("good.morning.message", null, 
									     LocaleContextHolder.getLocale());
	}
}
