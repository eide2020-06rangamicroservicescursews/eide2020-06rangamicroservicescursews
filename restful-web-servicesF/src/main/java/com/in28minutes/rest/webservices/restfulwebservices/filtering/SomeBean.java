package com.in28minutes.rest.webservices.restfulwebservices.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//first of all this is a static filtering.
//first choice to ignore properties in the service response:
//public class SomeBean {
//	private String field1;
//	private String field2;
//	
//	@JsonIgnore //this annotation will exclude field3 from the response.
//	private String field3;
	
//second choice to ignore properties in the service response:

//@JsonIgnoreProperties(value= {"field1", "field2"}) 
//the previous annotation was commented for lesson 25 (Dynamic filtering).
@JsonFilter("SomeBeanFilter")
public class SomeBean {
		private String field1;
		private String field2;		
		private String field3;	
	
	public SomeBean(String field1, String field2, String field3) {
		super();
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}
}
