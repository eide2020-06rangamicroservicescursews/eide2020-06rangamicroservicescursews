package com.in28minutes.rest.webservices.restfulwebservices.filtering;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;
/*note: if you don't prefix the package name with
 * 'com.in28minutes.rest.webservices.restfulwebservices.'
 * you'll have troubles running the service.
 */
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class FilteringController {
		
//	@GetMapping("/filtering")
//	public SomeBean retrieveSomeBean() {		
//		return new SomeBean("valu1", "value2","value3");	
//	}
	
	@GetMapping("/filtering")	
	//I want to send field1 and field2. (for the dynamic filtering).
	public MappingJacksonValue retrieveSomeBean() {		
		SomeBean someBean = new SomeBean("valu1", "value2","value3");
		
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter
				.filterOutAllExcept("field1", "field2");
		
		FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(someBean);
		mapping.setFilters(filters);
		return mapping;
	}
	
//	@GetMapping("/filtering-list")
//	public List<SomeBean> retrieveListOfSomeBeans() {
//		return Arrays.asList(new SomeBean("valu1", "value2","value3"),
//							 new SomeBean("valu12", "value22","value32"));
//	}
	
	//I want to send field2 and field3. (for the dynamic filtering).
	@GetMapping("/filtering-list")
	public MappingJacksonValue retrieveListOfSomeBeans() {
		List<SomeBean> list = Arrays.asList(new SomeBean("valu1", "value2","value3"),
							 				new SomeBean("valu12", "value22","value32"));
		
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter
				.filterOutAllExcept("field2", "field3");
		
		FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(list);
		mapping.setFilters(filters);
		return mapping;
	}
}
