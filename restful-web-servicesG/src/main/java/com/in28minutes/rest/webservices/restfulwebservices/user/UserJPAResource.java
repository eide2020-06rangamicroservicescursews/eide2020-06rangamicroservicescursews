package com.in28minutes.rest.webservices.restfulwebservices.user;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class UserJPAResource {

//	@Autowired
//	private UserDAOService service;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	PostRepository postRepository;
	
	
	//GET /users
	//retrieveAllUsers
//	@GetMapping("/jpa/users")
//	public List<User> retrieveAllUsers() {
//		return service.findAll();
//	}
	
	//lesson 51
	@GetMapping("/jpa/users")
	public List<User> retrieveAllUsers() {
		return userRepository.findAll();
	}
	
	//GET /users/{id}
	//retrieveUser(int id)
//	@GetMapping("/jpa/users/{id}")
//	public EntityModel<User> retrieveUser(@PathVariable int id) {
//		User user = service.findOne(id);
//		
//		if(user==null)
//			throw new UserNotFoundException("id-"+ id);
//		
//		
//		//"all-users", SERVER_PATH + "/users"
//		//retrieveAllUsers
//		EntityModel<User> resource = EntityModel.of(user);
//		
//		WebMvcLinkBuilder linkTo = 
//				linkTo(methodOn(this.getClass()).retrieveAllUsers());
//		
//		resource.add(linkTo.withRel("all-users"));
//		
//		//HATEOAS
//		
//		return resource;
//	}
	
	//lesson 51
	@GetMapping("/jpa/users/{id}")
	public EntityModel<User> retrieveUser(@PathVariable int id) {
		Optional<User> user = userRepository.findById(id);
		
		if(!user.isPresent())
			throw new UserNotFoundException("id-"+ id);
		
		
		//"all-users", SERVER_PATH + "/users"
		//retrieveAllUsers
		EntityModel<User> resource = EntityModel.of(user.get());
		
		WebMvcLinkBuilder linkTo = 
				linkTo(methodOn(this.getClass()).retrieveAllUsers());
		
		resource.add(linkTo.withRel("all-users"));
		
		//HATEOAS
		
		return resource;
	}	
	/*
	//working with POST (the goal is to create an user)
	//input - details of the user (this is sent with the request).
	//output - CREATED (waited status) & Return the created URI.
	//this version only creates de user, it doesn't send a result status.

	 * @PostMapping("/users") public void createUser(@RequestBody User user) { User
	 * savedUser = service.save(user); }
	 */
	
//	@PostMapping("/jpa/users")
//	//adding validation.
//	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
//		User savedUser = userRepository.save(user);
//		//created (status for /user/{} savedUser.getId().... = 4)
//		
//		URI location = ServletUriComponentsBuilder
//						.fromCurrentRequest()
//						.path("/{id}")
//						.buildAndExpand(savedUser.getId()).toUri();
//		return ResponseEntity.created(location).build();
//	}
	
//	@DeleteMapping("/jpa/users/{id}")
//	public void deleteUser(@PathVariable int id) {
//		User user = service.deleteById(id);
//		if (user == null) {
//			throw new UserNotFoundException("id-" + id);
//		}			
//	}

	//lesson 52
	@DeleteMapping("/jpa/users/{id}")
	public void deleteUser(@PathVariable int id) {
		userRepository.deleteById(id);					
	}
	
	//lesson 54.
	@PostMapping("/jpa/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = userRepository.save(user);
				
		URI location = ServletUriComponentsBuilder
						.fromCurrentRequest()
						.path("/{id}")
						.buildAndExpand(savedUser.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@GetMapping("/jpa/users/{id}/posts")
	public List<Post> retrieveAllUsers(@PathVariable int id) {
		Optional<User> userOptional = userRepository.findById(id);
		if(!userOptional.isPresent()) {
			throw new UserNotFoundException("id-" + id);
		}
		
		return userOptional.get().getPosts();
	}
	
	//lesson 55.
		@PostMapping("/jpa/users/{id}/posts")
		public ResponseEntity<Object> createPost(@PathVariable int id, @RequestBody Post post) {
			
			Optional<User> userOptional = userRepository.findById(id);
			
			if(!userOptional.isPresent()) {
				throw new UserNotFoundException("id-" + id);
			}
			
			User user = userOptional.get();
			
			post.setUser(user);
			
			postRepository.save(post);
			
					
			URI location = ServletUriComponentsBuilder
							.fromCurrentRequest()
							.path("/{id}")
							.buildAndExpand(post.getId())
							.toUri();
			return ResponseEntity.created(location).build();
		}
}
